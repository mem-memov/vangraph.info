var express = require('express');
var store = require('./store');
var scriptless = require('./scriptless');

var app = express();

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:1841');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/count/node/', function(req, res){

    store.countNodes(function(result) {
        
        res.send(''+result);
        
    });
    
    
});

app.get('/fetch/clips/of/word/:word/limit/height/:height/', function(req, res){

    store.fetchClipsWithWord(req.params.word, req.params.height, function(result) {

        res.send(result);
        
    });
    
    
});

app.get('/fetch/clip/:id/', function(req, res){

    store.fetchClip(req.params.id, function(result) {

        res.send({
            clip: result
        });
        
    });
    
    
});

app.get('/prompt/input/:letters/', function(req, res){
    
    store.fetchWordsByFirstLetters(req.params.letters, function(result) {
        
        res.send(result);
        
    });
    
});

app.get('/fetch/words/of/clip/:clip/', function(req, res){

    store.fetchWordsOfClip(req.params.clip, function(result) {

        res.send(result);
        
    });
    
    
});

app.get('/nojs/', function(req, res){
console.log(222)
    var query = req.originalUrl.substring('/nojs/?_escaped_fragment_='.length);
    
    scriptless.buildHtmlForQuery(query, function(html) {
        
        res.send(html);
        
    });
    

});

app.listen(3000);
console.log('Listening on port 3000');
