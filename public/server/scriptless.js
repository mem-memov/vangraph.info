var store = require('./store');
var jade = require('jade');

exports.buildHtmlForQuery = function(query, callback) {
    
        if (query === '') {
            buildIndexPage(callback);
            return;
        }
    
        if (query.length === 32 && /\d/.test(query)) {

            buildClipPage(query, callback);

        } else {

            buildWordPage(query, callback);

        }
    
};

function buildIndexPage(callback) {
    
    store.fetchDictionaryPart(function(dictionary, letters) {
        
        var options = {
            dictionary: dictionary,
            letters: letters
        };
        
        jade.renderFile('./template/index.jade', options, function(error, html) {

            if (error) throw error;

            callback(html);

        });
        
    });

}

function buildClipPage(query, callback) {
    
    store.fetchWordsOfClip(query, function(words) {
        
        var options = {
            words: words
        };

        jade.renderFile('./template/clip.jade', options, function(error, html) {

            if (error) throw error;

            callback(html);

        });
        
    });

}

function buildWordPage(query, callback) {
    
    store.fetchClipsWithWord(query, 40, function(clips) {
        
        var options = {
            clips: clips
        };

        jade.renderFile('./template/word.jade', options, function(error, html) {

            if (error) throw error;

            callback(html);

        });
        
    });
    
}

