var neo4j = require('./neo4j');
var database = neo4j.getDatabase('vangraph.info', 7474);


exports.countNodes = function(callback) {
    
    var query = {
        query: 'MATCH (n) RETURN count(n);',
        params: {}
    };
    
    database.query(query, function(result) {

        callback(result.data[0][0]);

    });
    
};

exports.fetchClipsWithWord = function(word, height, callback) {

    var query = {
        query: 'MATCH (word:word { string: {word} })<-[:clip_word]-(clip:clip { height: {height} }) WITH clip LIMIT 20 RETURN clip;',
        params: {
            word: word,
            height: parseInt(height, 10)
        }
    };

    database.query(query, function(result) {

        var clip, clips = [];
        
        result.data.forEach(function(collection) {
            
            clip = collection[0].data;

            clips.push({
                id: clip.id,
                file: makeClipUrl(clip)
            });
            
        });
        
        callback(clips);

    });
    
};

exports.fetchClip = function(id, callback) {

    var query = {
        query: 'MATCH (clip:clip { id: {id} }) RETURN clip;',
        params: {
            id: id
        }
    };

    database.query(query, function(result) {

        var clip, output;

        if (result.data[0] && result.data[0][0]) {
            clip = result.data[0][0].data;
            output = {
                id: clip.id,
                file: makeClipUrl(clip)
            };
        } else {
            output = null;
        }
        
        callback(output);

    });
    
};

exports.fetchWordsByFirstLetters = function(letters, callback) {
    
    
    var query = {
        query: 'MATCH (word:word) WHERE left(word.string, {length})={letters} RETURN DISTINCT left(word.string, {prompt_length}) AS prompt ORDER BY prompt ASC LIMIT 30;',
        params: {
            letters: letters,
            length: letters.length,
            prompt_length: letters.length+1
        }
    };
    
    database.query(query, function(result) {
        
        var prompts = [];
        
        result.data.forEach(function(collection) {
            
           prompts.push({
               prompt: collection[0]
           });
            
        });
        
        callback(prompts);
        
    });
    
};

exports.fetchWordsOfClip = function(clip, callback) {
    
    var query = {
        query: 'MATCH (:clip {id: {id} })-[:clip_word]->(word:word) WITH word LIMIT 20 RETURN word ORDER BY word.position ASC;',
        params: {
            id: clip
        }
    };
    
    database.query(query, function(result) {
        
        var word, words = [];
        
        result.data.forEach(function(collection) {
            
            word = collection[0].data;
            
            words.push({
                id: word.string
            });
            
        });
        
        callback(words);
        
    });
    
};

exports.fetchDictionaryPart = function(callback) {
    
    
    var query = {
        query: 'MATCH (word:word) RETURN DISTINCT word.string ORDER BY word.string ASC;',
        params: {
        }
    };
    
    database.query(query, function(result) {
        
        var word, words = [], dictionary = [], letter, letters = [];
        
        result.data.forEach(function(collection) {
            
            word = collection[0];
            
            if (!letter) {
                letter = word.substring(0, 1);
            } else if (letter !== word.substring(0, 1)) {
                dictionary.push(words);
                words = [];
                letters.push(letter);
                letter = word.substring(0, 1);
            }
            
            words.push(word);
            
        });

        callback(dictionary, letters);
        
    });
    
};

function makeClipUrl(clip) {
    
    return '/video/'+clip.directory+'/h'+clip.height+'/'+clip.language+'/'+clip.file;
    
}

function secondsToTime(seconds) {
	var hours = Math.floor(seconds / 3600);
	var minutes = Math.floor((seconds - hours*3600) / 60);
	var rest = seconds - hours*3600 - minutes * 60;
	return (hours < 10 ? '0'+hours : ''+hours) + '_' + (minutes < 10 ? '0'+minutes : ''+minutes) + '_' + (rest < 10 ? '0'+rest : ''+rest);
}