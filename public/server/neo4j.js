var http = require('http');
var databases = [];

function Database(server, port) {
    
    this.server = server;
    this.port = port;
    
}
Database.prototype.query = function(parameterizedCypherObject, callback) {

    var parameterizedCypherString = JSON.stringify(parameterizedCypherObject);
    
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': parameterizedCypherString.length,
        'Transfer-Encoding': 'chunked'
    };
    
    var options = {
        host: this.server,
        port: this.port,
        path: '/db/data/cypher',
        method: 'POST',
        headers: headers
    };
    
    var req = http.request(options, function(res) {
        
        res.setEncoding('utf-8');

        var responseString = '';

        res.on('data', function(data) {
            responseString += data;
        });

        res.on('end', function() {
            callback(JSON.parse(responseString));
        });
        
        res.on('error', function(error) {
            console.log(error);
        });
        
    });

    req.on('error', function(e) {
        console.log(e);
        // TODO: handle error.
    });
    
    req.write(parameterizedCypherString);
    req.end();
    
    
};

function getDatabase(server, port) {
    
    databases.forEach(function(database) {
        
        if (database.isIdentified(server, port)) {
            
            return database;
            
        }
        
    });
    
    var database = new Database(server, port);
    databases.push(database);
    
    return database;
    
}

module.exports = {
    getDatabase: getDatabase
};
        
        