Ext.define('Vangraph.Application', {
    name: 'Vangraph',

    extend: 'Ext.app.Application',
    
    requires: [
        'Ext.util.History', // v4.2.1.883 Uncaught TypeError: Cannot read property 'fieldId' of undefined 
        'Ext.data.proxy.Rest', // v4.2.1.883 [Ext.Loader] Synchronously loading...
        'Ext.form.field.ComboBox', // v4.2.1.883 [Ext.Loader] Synchronously loading...
        'Ext.draw.Component', // v4.2.1.883 [Ext.Loader] Synchronously loading...
        'Ext.util.Point' // v4.2.1.883 http://www.sencha.com/forum/showthread.php?259304-Error-in-onMouseEnter-on-Sprites
    ],

    views: [
        'Vangraph.view.ClipGraph',
        'Vangraph.view.ToolPanel',
        'Vangraph.view.SearchForm',
        'Vangraph.view.WordList',
        'Vangraph.view.ClipPlayer',
        'Vangraph.view.ux.vangraph.vertice.Word',
        'Vangraph.view.ux.vangraph.vertice.Clip',
        'Vangraph.view.ux.vangraph.edge.Line'
    ],

    controllers: [
        'Vangraph.controller.ViewportController',
        'Vangraph.controller.ClipGraphController',
        'Vangraph.controller.ToolPanelController',
        'Vangraph.controller.SearchFormController',
        'Vangraph.controller.WordListController',
        'Vangraph.controller.ClipPlayerController'
    ],

    stores: [
        'Vangraph.store.SearchPromptStore',
        'Vangraph.store.WordStore'
    ],
    
    models: [
        'Vangraph.model.SearchPromptModel',
        'Vangraph.model.WordModel'
    ]
});

