Ext.define('Vangraph.view.ClipPlayer', {
    
    extend: 'Ext.Component',
    
    alias: 'widget.clip-player',

    floating: {
        shadow: true,
        shadowOffset: 20
    },
    toFrontOnShow: true,
    autoShow: false,

    initComponent: function() {
        
        var me = this;
        
        me.hidden = true;

        me.renderTpl = [
            '<video id="{id}-clip-player" class="clip-player" controls style="height: 480px; width: 800px; background-color: black;">',
            '<source src="{url}" type="video/mp4" />',
            '</video>'
        ],

        me.renderData = {
            url: '#'
        },
                
        me.renderSelectors = {
            video: 'video'
        };
        
        me.listeners = {
            ended: {
                element: 'video',
                fn: function(){ 
                    me.hide();
                    me.fireEvent('clip-player-hidden', me);
                }
            },
            play: {
                element: 'video',
                fn: function(){ 
                    var domElement = Ext.getDom(this);
                    Ext.Array.each(Ext.DomQuery.select('video'), function(video) {
                        if (video !== domElement) {
                            video.pause();
                            video.currentTime = 0;
                        }
                    });
                }
            },
            afterrender: function(me) {
                
            }
        };

        me.callParent(arguments);
        
    },
    
    play: function(url) {

        this.show();
        var domVideo = Ext.getDom(this.video);
        domVideo.setAttribute('src', url);
        domVideo.play();

    },
    
    stop: function() {

        var domVideo = Ext.getDom(this.video);
        domVideo.currentTime = 0;
        domVideo.pause();
        this.hide();
        
    }
    
});