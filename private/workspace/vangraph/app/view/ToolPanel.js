Ext.define('Vangraph.view.ToolPanel', {
    
    extend: 'Ext.panel.Panel',
    
    alias: 'widget.tool-panel',

    frame: true,
    
    floating: {
        shadow: true,
        shadowOffset: 20
    },
    
    width: 200,
    
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    
    initComponent: function() {
        
        this.items = [
            {
                xtype: 'search-form'
            },
            {
                xtype: 'word-list',
                hidden: true
            }
        ];

        this.listeners = {
            afterrender: function(panel){
                panel.setPosition(10,10, true);
            },                    
            mouseover: {
                element: 'body',
                fn: function() {
                    this.fireEvent('mouse-over-tool-panel', this);
                },
                scope: this
            },                    
            mouseout: {
                element: 'body',
                fn: function() {
                    this.fireEvent('mouse-out-of-tool-panel', this);
                },
                scope: this
            }
        };
        
        this.callParent();
        
    }
    

    
});