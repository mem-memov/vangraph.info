Ext.define('Vangraph.view.Viewport', {
    
    extend: 'Ext.container.Viewport',

    layout: 'fit',
    
    items: [
        {
            xtype: 'clip-graph'
        },
        {
            xtype: 'tool-panel',
            autoShow: true
        },
        {
            xtype: 'clip-player'
        }
    ]
    
});