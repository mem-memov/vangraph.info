Ext.define('Vangraph.view.SearchForm', {
    
    extend: 'Ext.form.Panel',
    
    alias: 'widget.search-form',
    
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    
    border: false,
    bodyStyle: {
        backgroundColor: 'inherit'
    },
    
    items: [
        {
            xtype: 'combobox',
            itemId: 'search-field',
            displayField: 'prompt',
            valueField: 'prompt',
            enableKeyEvents: true, 
            minChars: 1,
            store: {
                type: 'search-prompt-store'
            },
            typeAhead: true,
            hideTrigger: true,
            flex: 1,
            margin: '5 0 5 5',
            name: 'query',
            listConfig: {
                loadMask: false,
            }
        }, {
            xtype: 'button',
            itemId: 'submit-button',
            width: 40,
            text: 'ok',
            margin: '5 5 5 0'
        }
    ]
    
});