Ext.define('Vangraph.view.WordList', {
    
    extend: 'Ext.grid.Panel',
    
    alias: 'widget.word-list',

    //border: false,
    
    viewConfig: {
        stripeRows: true
    },
    columnLines: true,
    scroll: 'vertical',
    hideHeaders: true,
    maxHeight: 400,
    
    initComponent: function() {
        
        this.store = {
            type: 'word-store',
            autoLoad: true
        };
        
        this.selModel = {
            mode: 'SINGLE',
            allowDeselect: true,
            toggleOnClick: true
        };
        
        this.columns = [
            {
                dataIndex: 'word',
                flex: 1
            }
        ];
        
        this.callParent();
        
    }
    
});