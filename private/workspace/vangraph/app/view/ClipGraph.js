Ext.define('Vangraph.view.ClipGraph', {
    
    extend: 'Vangraph.view.ux.vangraph.Component',
    
    alias: 'widget.clip-graph',
    
    initComponent: function() {

        this.callParent(arguments);

    }
    
});