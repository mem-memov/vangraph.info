Ext.define('Vangraph.view.ux.vangraph.AbstractShape', {

    putOnSurface: function(surface) {

        surface.add(this.sprite);
        this.sprite.show(true);

    },

    getDraggableConfig: function(shape) {
        
        var shape = shape;
        
        return {
            onDrag: function(e) { 

                this.xy = e.xy; // see v4.2.1.883 Ext.draw.SpriteDD.prototype.onDrag

                shape.updatePhysics({
                    id: shape.id,
                    x: e.xy[0]+shape.boundary.x1,
                    y: e.xy[1]+shape.boundary.y1,
                    constrained: true
                });

            }
        };
        
    },
    
    isOut: function(x, y) {
        var boundary = this.boundary;
        return x<0 || y<0 || x>boundary.width || y>boundary.height;
    }
    
});