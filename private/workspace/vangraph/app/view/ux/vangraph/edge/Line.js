Ext.define('Vangraph.view.ux.vangraph.edge.Line', {
    
    extend: 'Vangraph.view.ux.vangraph.edge.Abstract',
    
    constructor: function(config) {
        
        this.from = config.from;
        this.to = config.to;
        
        this.x1 = 0;
        this.y1 = 0;
        this.x2 = 0;
        this.y2 = 0;
        
        this.sprite = Ext.create('Ext.draw.Sprite', {
            type: 'path',
            path: this.getPath(0,0,0,0),
            'stroke-width': 1,
            //opacity: 0.3,
            stroke: config.stroke ? config.stroke : 'green'
        });
    
    },
    
    putOnSurface: function(surface) {

        surface.add(this.sprite);
        this.sprite.show(true);
        
    },
    
    getFromId: function() {
        return this.from;
    },
    getToId: function() {
        return this.to;
    },
    
    draw: function(x1, y1, x2, y2) {
        
        if (this.x1 === x1 && this.y1 === y1 && this.x2 === x2 && this.y2 === y2) {
            return;
        }
        
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;

        var drawX1 = Math.floor(0+x1 - this.boundary.x1);
        var drawY1 = Math.floor(0+y1 - this.boundary.y1);
        var drawX2 = Math.floor(0+x2 - this.boundary.x1);
        var drawY2 = Math.floor(0+y2 - this.boundary.y1);

//        if (this.isOut(drawX1, drawY1) || this.isOut(drawX2, drawY2)) {
//            return;
//        }

        this.sprite.setAttributes({ 
            path: this.getPath(drawX1, drawY1, drawX2, drawY2)
        }, true);

    },
    
    getPath: function(x1, y1, x2, y2) {

        return "M "+x1+" "+y1+" L "+x2+" "+y2+"";
        
    }
    
    
});