Ext.define('Vangraph.view.ux.vangraph.edge.Abstract', {
    
    extend: 'Vangraph.view.ux.vangraph.AbstractShape',
    
    recognize: function(from, to) {
        
        return this.from === from && this.to === to;
        
    }
    
});