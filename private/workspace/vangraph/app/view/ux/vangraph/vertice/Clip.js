Ext.define('Vangraph.view.ux.vangraph.vertice.Clip', {
    
    extend: 'Vangraph.view.ux.vangraph.vertice.Abstract',
    
    alias: 'widget.vangraph-vertice-clip',

    constructor: function(config) {
        
        var me = this;
        
        me.isPlaying = false;
        me.positionFrosen = false; // mouse can leave because another floating element covers this one. This flag helps cope with the problem.
        
        me.id = config.id;
        me.centerX = config.x;
        me.centerY = config.y;
        me.width = config.width ? config.width : 50;
        me.height = config.height ? config.height : 40;
        me.url = config.file;
        if (window.location.hostname === 'localhost') { // in sencha workspace - localhost:1841
            me.url = 'http://vangraph.info' +  me.url;
        }
        
        me.updatePhysics = config.updatePhysics;
        
        me.clip = Ext.create('Ext.Component', {
            width: me.width,
            height: me.height,
            floating: true,
            draggable: me.getDraggableConfig(me),
            renderTpl: [
                '<video id="{id}-video" style="height: 40px; background-color: black;">',
                '<source src="{url}" type="video/mp4">',
                '</video>'
            ],
            renderData: {
                url: me.url
            },
            childEls: ['video'],
            listeners: {
                click: {
                    element: 'video',
                    fn: function(){ 
                       config.fireComponentEvent('shapeclick', me);
                    }
                },
                dblclick: {
                    element: 'video',
                    fn: function(){ 
                        me.positionFrosen = true;
                        me.pause();
                        config.fireComponentEvent('shapedblclick', me);
                    }
                },
                ended: {
                    element: 'video',
                    fn: function(){ 
                        me.isPlaying = false;
                    }
                },
                pause: {
                    element: 'video',
                    fn: function(){ 
                        me.isPlaying = false;
                    }
                },
                play: {
                    element: 'video',
                    fn: function(){ 
                        var domElement = Ext.getDom(this);
                        Ext.Array.each(Ext.DomQuery.select('video'), function(video) {
                            if (video !== domElement && video.currentTime) {
                                video.currentTime = 0;
                                video.pause();
                            }
                        });
                    }
                },
                mouseover: {
                    element: 'el',
                    fn: function(event){ 
                        me.clip.toFront(); // prevent other clips from bumping this one out
                        me.positionFrosen = false;
                        me.updatePhysics({
                            id: me.id,
                            constrained: true,
                            x: me.centerX,
                            y: me.centerY
                        });
                        config.fireComponentEvent('shapemouseover', me);
                    }
                },
                mouseout: {
                    element: 'el',
                    fn: function(event){ 
                        if (!me.positionFrosen) {
                            me.updatePhysics({
                                id: me.id,
                                constrained: false,
                                x: me.centerX,
                                y: me.centerY
                            });
                        }
                        config.fireComponentEvent('shapemouseout', me);
                    }
                }
            },
            border: false,
            style: {
                borderColor: 'red',
                borderStyle: 'solid'
            }
        });
        
    },
    
    highlight: function() {
        
        this.clip.setBorder(3);
        
    },
    
    unhighlight: function() {
        
        if (this.isPlaying) {
            return;
        }
        
        this.clip.setBorder(false);
        
    },
    
    putOnSurface: function(surface) {

        this.surface = surface;
        this.clip.show();

    },
    
    provideNearestCoordinates: function(x, y) {

        var dx = x - this.centerX;
        var dy = y - this.centerY;
        var dxToDyRatio = dx / dy;
        var absoluteDxToDyRatio = Math.abs(dxToDyRatio);
        var widthToHeightRatio = this.width / this.height;
        var halfHeight = this.height / 2;
        var halfWidth = this.width / 2;
        
        var ratio = 1;
        
        if (absoluteDxToDyRatio < widthToHeightRatio && dy < 0) {
            
            ratio = - halfHeight / dy;
            
        } else if (absoluteDxToDyRatio < widthToHeightRatio && dy > 0) {
            
            ratio = halfHeight / dy;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx < 0) {
            
            ratio = - halfWidth / dx;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx > 0) {
            
            ratio = halfWidth / dx;
            
        }
        
        var nearestX = this.centerX + dx * ratio;
        var nearestY = this.centerY + dy * ratio;

        return [nearestX, nearestY]; 
        
    },
    
    draw: function(x, y) {

        x = Math.floor(x);
        y = Math.floor(y);
        
        this.centerX = x;
        this.centerY = y;
        
        var drawX = x - this.width/2 - this.boundary.x1;
        var drawY = y - this.height/2 - this.boundary.y1;

        //if (!this.isOut(drawX, drawY)) {
            this.clip.setPosition(drawX + this.boundary.pageX, drawY + this.boundary.pageY);
        //}

    },
    
    play: function() {

        this.highlight();
        Ext.getDom(this.clip.video).play();
        this.isPlaying = true;
        
    },
    
    pause: function() {

        this.unhighlight();
        Ext.getDom(this.clip.video).pause();
        this.isPlaying = false;
        
    },
    
    togglePlayBack: function() {

        if (this.isPlaying) {
            this.pause();
        } else {
            this.play();
        }
        
    }
    
});