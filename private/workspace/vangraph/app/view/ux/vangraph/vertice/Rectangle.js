Ext.define('Vangraph.view.ux.vangraph.vertice.TestRectangle', {
    
    extend: 'Vangraph.view.ux.vangraph.vertice.Abstract',
    
    constructor: function(config) {
        
        var me = this;
        
        me.id = config.id;
        me.centerX = config.x;
        me.centerY = config.y;
        me.width = config.width;
        me.height = config.height;
        
        me.updatePhysics = config.updatePhysics;

        me.sprite = Ext.create('Ext.draw.Sprite', {
            type: 'rect',
            x: me.centerX - me.width / 2,
            y: me.centerY - me.height / 2,
            width: config.width,
            height: config.height,
            fill: config.fill,
            stroke: config.stroke,
            draggable: me.getDraggableConfig(me),
            listeners: {
                click: function(extElement, mouseEvent) {
                    config.fireComponentEvent('shapeclick', me);
                }
            }
        });

    },
    
    putOnSurface: function(surface) {

        surface.add(this.sprite);
        this.sprite.show(true);
        
    },
    
    provideNearestCoordinates: function(x, y) {
        
        var dx = x - this.centerX;
        var dy = y - this.centerY;
        var dxToDyRatio = dx / dy;
        var absoluteDxToDyRatio = Math.abs(dxToDyRatio);
        var widthToHeightRatio = this.width / this.height;
        var halfHeight = this.height / 2;
        var halfWidth = this.width / 2;
        
        var ratio = 1;
        
        if (absoluteDxToDyRatio < widthToHeightRatio && dy < 0) {
            
            ratio = - halfHeight / dy;
            
        } else if (absoluteDxToDyRatio < widthToHeightRatio && dy > 0) {
            
            ratio = halfHeight / dy;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx < 0) {
            
            ratio = - halfWidth / dx;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx > 0) {
            
            ratio = halfWidth / dx;
            
        }
        
        var nearestX = this.centerX + dx * ratio;
        var nearestY = this.centerY + dy * ratio;

        return [nearestX, nearestY]; 
        
    },
    
    draw: function(x, y) {

        x = Math.floor(x);
        y = Math.floor(y);
        
        if (this.centerX === x && this.centerY === y) {
            return;
        }

        this.centerX = x;
        this.centerY = y;
        
        if (!this.isOut(x, y)) {
            this.sprite.setAttributes({
                x: this.centerX - this.width / 2,
                y: this.centerY - this.height / 2
            }, true);
        }

        
    }

    
    
});