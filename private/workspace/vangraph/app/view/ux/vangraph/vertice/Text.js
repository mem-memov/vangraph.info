Ext.define('Vangraph.view.ux.vangraph.vertice.Text', {
    
    extend: 'Vangraph.view.ux.vangraph.vertice.Abstract',

    constructor: function(config) {
        
        var me = this;
        
        me.id = config.id;
        me.centerX = config.x;
        me.centerY = config.y;
        
        me.selected = (typeof config.selected !== 'undefined') ? !!config.selected : false;
     
        
        me.updatePhysics = config.updatePhysics;
        
        me.frameSprite = Ext.create('Ext.draw.Sprite', {
            type: 'rect',
            fill: config.fill,
            opacity: 0,
            draggable: me.getDraggableConfig(me),
            listeners: {
                click: function(extElement, mouseEvent) {
                    config.fireComponentEvent('shapeclick', me);
                }
            }
        });

        me.sprite = Ext.create('Ext.draw.Sprite', {
            type: 'text',
            text: config.text,
            fill: config.fill,
            stroke: config.stroke
        });


    },
    
    putOnSurface: function(surface) {

        surface.add(this.sprite);
        this.sprite.show(true);
        
        var box = this.sprite.getBBox();
        
        this.width = box.width;
        this.height = box.height;
        this.centerX = box.x + box.width / 2;
        this.centerY = box.y;
        
        this.frameSprite.setAttributes({
            x: box.x,
            y: box.y - box.height / 2,
            width: box.width,
            height: box.height
        });
        
        surface.add(this.frameSprite);
        this.frameSprite.show(true);
        
    },
    
    provideNearestCoordinates: function(x, y) {
        
        var dx = x - this.centerX;
        var dy = y - this.centerY;
        var dxToDyRatio = dx / dy;
        var absoluteDxToDyRatio = Math.abs(dxToDyRatio);
        var widthToHeightRatio = this.width / this.height;
        var halfHeight = this.height / 2;
        var halfWidth = this.width / 2;
        
        var ratio = 1;
        
        if (absoluteDxToDyRatio < widthToHeightRatio && dy < 0) {
            
            ratio = - halfHeight / dy;
            
        } else if (absoluteDxToDyRatio < widthToHeightRatio && dy > 0) {
            
            ratio = halfHeight / dy;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx < 0) {
            
            ratio = - halfWidth / dx;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx > 0) {
            
            ratio = halfWidth / dx;
            
        }
        
        var nearestX = this.centerX + dx * ratio;
        var nearestY = this.centerY + dy * ratio;

        return [nearestX, nearestY]; 
        
    },
    
    draw: function(x, y) {

        x = Math.floor(x);
        y = Math.floor(y);
        
        if (this.centerX === x && this.centerY === y) {
            return;
        }
        
        this.centerX = x;
        this.centerY = y;

        if (!this.isOut(x, y)) {
            this.sprite.setAttributes({
                x: this.centerX - this.width / 2,
                y: this.centerY
            }, true);

            this.frameSprite.setAttributes({
                x: this.centerX - this.width / 2,
                y: this.centerY - this.height / 2
            }, true);
        }
//                    time = Math.floor(time*1000);
//                    console.log(time);
//                    component['sprite'+id].animate({
//                        to: {
//                            x: x,
//                            y: y
//                        },
//                        duration: time,
//                        easing: 'linear'
//                    });
        
    }

    
    
});