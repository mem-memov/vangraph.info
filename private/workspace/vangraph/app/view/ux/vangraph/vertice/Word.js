Ext.define('Vangraph.view.ux.vangraph.vertice.Word', {
    
    extend: 'Vangraph.view.ux.vangraph.vertice.Abstract',
    
    alias: 'widget.vangraph-vertice-word',

    constructor: function(config) {
        
        var me = this;
        
        me.initialConfiguration = config;
        
        me.id = config.id;
        
        // sprites are initially hidden if not specified otherwise
        me.centerX = config.x ? config.x : -100;
        me.centerY = config.y ? config.y : -100;
        
        me.selected = (typeof config.selected !== 'undefined') ? !!config.selected : false;
     
        
        me.updatePhysics = config.updatePhysics;
        
        me.frameSprite = Ext.create('Ext.draw.Sprite', {
            type: 'rect',
            fill: config.fill,
            opacity: 0,
            draggable: me.getDraggableConfig(me),
            listeners: {
                click: function(extElement, mouseEvent) {
                    config.fireComponentEvent('shapeclick', me);
                },
                mouseover: {
                    element: 'el',
                    fn: function(event){ 
                        me.updatePhysics({
                            id: me.id,
                            constrained: true,
                            x: me.centerX,
                            y: me.centerY
                        });
                        config.fireComponentEvent('shapemouseover', me);
                    }
                },
                mouseout: {
                    element: 'el',
                    fn: function(event){ 
                        if (!me.positionFrosen) {
                            me.updatePhysics({
                                id: me.id,
                                constrained: false,
                                x: me.centerX,
                                y: me.centerY
                            });
                        }
                        config.fireComponentEvent('shapemouseout', me);
                    }
                }
            }
        });

        me.sprite = Ext.create('Ext.draw.Sprite', {
            type: 'text',
            text: config.text,
            fill: config.fill,
            stroke: config.stroke
        });
        


    },
    
    highlight: function() {
        
        this.sprite.setAttributes({
            fill: '#B40404'
        }, true);
        
        this.sprite.setStyle({
            fontWeight: 'bold'
        });
        
    },
    
    unhighlight: function() {
        
        this.sprite.setAttributes({
            fill: this.initialConfiguration.fill
        });
        
        this.sprite.setStyle({
            fontWeight: 'normal'
        }, true);
        
    },
    
    putOnSurface: function(surface) {

        surface.add(this.sprite);
        this.sprite.show(true);
        
        var box = this.sprite.getBBox();
        
        this.width = box.width;
        this.height = box.height;
        this.centerX = box.x + box.width / 2;
        this.centerY = box.y;
        
        this.frameSprite.setAttributes({
            x: box.x,
            y: box.y - box.height / 2,
            width: box.width,
            height: box.height
        });
        
        surface.add(this.frameSprite);
        this.frameSprite.show(true);
        
    },
    
    provideNearestCoordinates: function(x, y) {
        
        var dx = x - this.centerX;
        var dy = y - this.centerY;
        var dxToDyRatio = dx / dy;
        var absoluteDxToDyRatio = Math.abs(dxToDyRatio);
        var widthToHeightRatio = this.width / this.height;
        var halfHeight = this.height / 2;
        var halfWidth = this.width / 2;
        
        var ratio = 1;
        
        if (absoluteDxToDyRatio < widthToHeightRatio && dy < 0) {
            
            ratio = - halfHeight / dy;
            
        } else if (absoluteDxToDyRatio < widthToHeightRatio && dy > 0) {
            
            ratio = halfHeight / dy;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx < 0) {
            
            ratio = - halfWidth / dx;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx > 0) {
            
            ratio = halfWidth / dx;
            
        }
        
        var nearestX = this.centerX + dx * ratio;
        var nearestY = this.centerY + dy * ratio;

        return [nearestX, nearestY]; 
        
    },
    
    draw: function(x, y) {
        
        if (isNaN(x) || isNaN(y)) {
            return;
        }
        
        this.centerX = x;
        this.centerY = y;
        
        var drawTextX = this.centerX - this.width / 2 - this.boundary.x1;
        var drawTextY = this.centerY - this.boundary.y1;
        
        var drawFrameX = this.centerX - this.width / 2 - this.boundary.x1;
        var drawFrameY = this.centerY - this.height / 2 - this.boundary.y1;

        //if (!this.isOut(drawFrameX, drawFrameY)) {
            
            this.sprite.setAttributes({
                x: drawTextX,
                y: drawTextY
            }, true);

            this.frameSprite.setAttributes({
                x: drawFrameX,
                y: drawFrameY
            }, true);
            
        //}
        
    }

});