Ext.define('Vangraph.view.ux.vangraph.vertice.Circle', {
    
    extend: 'Vangraph.view.ux.vangraph.vertice.Abstract',
    
    constructor: function(config) {
        
        var me = this;
        
        me.id = config.id;
        me.centerX = config.x ? config.x : 0;
        me.centery = config.y ? config.y : 0;
        me.radius = config.radius ? config.radius : 5;
        
        me.updatePhysics = config.updatePhysics;

        me.sprite = Ext.create('Ext.draw.Sprite', {
            type: 'circle',
            x: me.centerX,
            y: me.centery,
            radius: me.radius,
            fill: config.fill ? config.fill : 'yellow',
            stroke: config.stroke ? config.stroke : 'red',
            draggable: me.getDraggableConfig(me),
            listeners: {
                click: function(extElement, mouseEvent) {
                    config.fireComponentEvent('shapeclick', me);
                }
            }
        });

    },
    
    putOnSurface: function(surface) {

        surface.add(this.sprite);
        this.sprite.show(true);
        
    },
    
    provideNearestCoordinates: function(x, y) {

        var dx = x - this.centerX;
        var dy = y - this.centerY;
        var distance = Math.sqrt(dx*dx + dy*dy);
        var ratio = this.radius / distance;
        var nearestX = this.centerX + dx * ratio;
        var nearestY = this.centerY + dy *  ratio; 

        return [nearestX, nearestY];
        
    },
    
    draw: function(x, y) {
        
        if (id !== this.id) {
            return;
        }
        
        x = Math.floor(x);
        y = Math.floor(y);
        
        if (this.centerX === x && this.centerY === y) {
            return;
        }
        
        this.centerX = x;
        this.centerY = y;
        
        if (!this.isOut(x, y)) {
            this.sprite.setAttributes({
                x: x,
                y: y
            }, true);
        }
        
//                    time = Math.floor(time*1000);
//                    console.log(time);
//                    component['sprite'+id].animate({
//                        to: {
//                            x: x,
//                            y: y
//                        },
//                        duration: time,
//                        easing: 'linear'
//                    });
        
    }

    
    
});