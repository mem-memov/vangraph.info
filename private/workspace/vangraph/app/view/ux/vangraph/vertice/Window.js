Ext.define('Vangraph.view.ux.vangraph.vertice.Window', {
    
    extend: 'Vangraph.view.ux.vangraph.vertice.Abstract',

    constructor: function(config) {
        
        var me = this;
        
        me.id = config.id;
        me.centerX = config.x;
        me.centerY = config.y;
        me.width = config.width ? config.width : 50;
        me.height = config.height ? config.height : 40;
        me.maximized = false;
        
        me.updatePhysics = config.updatePhysics;
        
        me.wnd = Ext.create('Ext.panel.Panel', {
            title: ''+config.text,
            width: me.width,
            height: me.height,
            floating: true,
            header: {
                listeners: {
                    click: function(header) {
                        var wnd = header.up('panel');
                        
                        if (!me.maximized) {
                            wnd.setWidth(Ext.getBody().getWidth());
                            wnd.setHeight(500);
                            wnd.setPosition(0,0);
                            //
                            me.maximized = true;
                        } else {
                            wnd.setHeight(22);
                            me.maximized = false;
                        }
                    },
                    afterrender: function(header) {
                        var wnd = header.up('panel');
                        wnd.setHeight(22);
                    }
                }
            },
            layout: 'fit', 
            items: [{
                xtype: 'panel'
            }],
            listeners: {
                resize: function(wnd) {
                    me.width = wnd.getWidth();
                    me.height = wnd.getHeight();
                },
                move: function(wnd, x, y) {

//                    me.centerX = x + me.width/2;
//                    me.centerY = y + me.height/2;
                    
//                    me.updatePhysics({
//                        id: me.id,
//                        constrained: true,
//                        x: me.centerX,
//                        y: me.centerY
//                    });
                    
                }
            }
        });

    },
    
    putOnSurface: function(surface) {

        this.wnd.show();
        

    },
    
    provideNearestCoordinates: function(x, y) {
        
        var dx = x - this.centerX;
        var dy = y - this.centerY;
        var dxToDyRatio = dx / dy;
        var absoluteDxToDyRatio = Math.abs(dxToDyRatio);
        var widthToHeightRatio = this.width / this.height;
        var halfHeight = this.height / 2;
        var halfWidth = this.width / 2;
        
        var ratio = 1;
        
        if (absoluteDxToDyRatio < widthToHeightRatio && dy < 0) {
            
            ratio = - halfHeight / dy;
            
        } else if (absoluteDxToDyRatio < widthToHeightRatio && dy > 0) {
            
            ratio = halfHeight / dy;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx < 0) {
            
            ratio = - halfWidth / dx;
            
        } else if (absoluteDxToDyRatio > widthToHeightRatio && dx > 0) {
            
            ratio = halfWidth / dx;
            
        }
        
        var nearestX = this.centerX + dx * ratio;
        var nearestY = this.centerY + dy * ratio;

        return [nearestX, nearestY]; 
        
    },
    
    draw: function(x, y) {

        x = Math.floor(x);
        y = Math.floor(y);
        
        if (this.centerX === x && this.centerY === y) {
            return;
        }
        
        this.centerX = x;
        this.centerY = y;

        if (!this.maximized && !this.isOut(x, y)) {
             this.wnd.setPosition(x - this.width/2, y - this.height/2);
        }

    }
    
});