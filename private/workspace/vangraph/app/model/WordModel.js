Ext.define('Vangraph.model.WordModel', {
    
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'word', type: 'string' }
    ]
    
});