Ext.define('Vangraph.model.SearchPromptModel', {
    
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'prompt', type: 'string' }
    ]
    
});