Ext.define('Vangraph.controller.ClipPlayerController', {
    
    extend: 'Ext.app.Controller',
    
    init: function() {
        
        this.listen({
            component: {
                'clip-player': {
                    'show': this.onClipPlayerShown,
                    'hide': this.onClipPlayerHidden
                }
            },
            controller: {
                '*': {
                    'clip-playback-requested': this.onClipPlaybackRequested,
                    'before-clip-graph-animation': this.beforeClipGraphAnimation,
                    'player-removal-requested': this.onPlayerRemovalRequested
                }
            }
        });
        
    },
    
    onPlayerRemovalRequested: function() {
        
        var clipPlayer = Ext.ComponentQuery.query('clip-player')[0];
        
        clipPlayer.stop();
        
    },
    
    beforeClipGraphAnimation: function() {
        
        var clipPlayer = Ext.ComponentQuery.query('clip-player')[0];
        
        return clipPlayer.isHidden();
        
    },
    
    onClipPlayerShown: function(clipPlayer) {
        
        this.fireEvent('clip-player-shown');
        
    },
    
    onClipPlayerHidden: function(clipPlayer) {
        
        this.fireEvent('clip-player-hidden');
        
    },
    
    onClipPlaybackRequested: function(url) {

        var clipPlayer = Ext.ComponentQuery.query('clip-player')[0];
        clipPlayer.play(url);

    }
    
});