Ext.define('Vangraph.controller.ToolPanelController', {
    
    extend: 'Ext.app.Controller',
    
    init: function() {
        
        this.listen({
            component: {
                'tool-panel': {
                    'mouse-over-tool-panel': this.onMouseOverToolPanel,
                    'mouse-out-of-tool-panel': this.onMouseOutOfToolPanel
                }
            },
            controller: {
                '*': {
                    
                }
            }
        });
        
    },
    
    onMouseOverToolPanel: function(toolPanel) {
        
        toolPanel.down('word-list').show();        
        
    },
    
    onMouseOutOfToolPanel: function(toolPanel) {
        
        toolPanel.down('word-list').hide();
        
    }
    
});