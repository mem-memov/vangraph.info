Ext.define('Vangraph.controller.SearchFormController', {
    
    extend: 'Ext.app.Controller',
    
    init: function() {
        
        this.listen({
            component: {
                'search-form [itemId="submit-button"]': {
                    click: this.onSubmitButtonClick
                },
                'search-form [itemId="search-field"]': {
                    keypress: this.onKeyPressWhenInSearchField,
                    afterrender: this.onSearchFieldRendered
                }
            },
            controller: {
                '*': {
                    'clip-click': this.onClipClick,
                    'word-click': this.onWordClick
                }
            }
        });
        
    },
    
    onLaunch: function() {
        
        // initialize browser history
        
        Ext.DomHelper.append(Ext.getBody(), {tag: 'input', type:'hidden', id: Ext.History.fieldId});
        Ext.DomHelper.append(Ext.getBody(), {tag: 'iframe', style:'display: none;', id: Ext.History.iframeId});
        
        Ext.History.init(function() {

            Ext.History.on('change', function(query) {
                
                this.chooseQueryEvent(query.substring(1)); // skip !
            
            }, this);

        }, this);
        
        // show something on screen
        
        if (window.location.hash) {
            
            var query = location.hash.substring(2); // skip #!

            this.chooseQueryEvent(query);
            
        } else {
            
            this.chooseQueryEvent('magic');
            
        }
        
    },
    
    onWordClick: function(word) {
        
        this.changeHistory(word);
        
    },
    
    onClipClick: function(clip) {

        this.changeHistory(clip);
        
    },
    
    onSubmitButtonClick: function(button) {
        
        var query = button.up('search-form').getValues().query;
        
        this.changeHistory(query);
        
    },
    
    onKeyPressWhenInSearchField: function(searchField, event) {
        
        if (event.button !== 12) {
            return;
        }
        
        var query = searchField.getValue();
        
        this.changeHistory(query);
        
    },
    
    onSearchFieldRendered: function(field) {
        
        setTimeout(function(){
            var input = Ext.DomQuery.selectNode('input', Ext.getDom(field.getEl()));
            input.focus();
        }, 500);
        
    },
    
    chooseQueryEvent: function(query) {
        
        if (query.length === 32 && /\d/.test(query)) {

            this.fireEvent('new-clip-query', query);

        } else {

            this.fireEvent('new-word-query', query);

        }
        
    },
    
    changeHistory: function(query) {
        
        // exclamation mark is here for SEO _escaped_fragment_ 
        // https://developers.google.com/webmasters/ajax-crawling/docs/specification
        // http://help.yandex.ru/webmaster/robot-workings/ajax-indexing.xml
        var newToken = '!' + query; 
        
        var oldToken = Ext.History.getToken();
        
        if (oldToken === null || oldToken.search(newToken) === -1) {
            
            Ext.History.add(newToken);
            
        } else { // the same action has been done repeatedly
            
            this.chooseQueryEvent(query);
            
        }
        
    }
    
});