Ext.define('Vangraph.controller.ViewportController', {
    
    extend: 'Ext.app.Controller',
    
    init: function() {
        
        this.listen({
            component: {

            },
            controller: {
                '*': {

                }
            }
        });
        
    }
    
});