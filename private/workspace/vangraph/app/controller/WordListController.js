Ext.define('Vangraph.controller.WordListController', {
    
    extend: 'Ext.app.Controller',
    
    init: function() {
        
        this.listen({
            component: {
                'word-list': {
                    select: this.onWordSelect,
                    deselect: this.onWordDeselect
                }
            },
            controller: {
                '*': {
                    'word-added': this.onWordAdded
                }
            }
        });
        
    },
    
    onWordSelect: function(selectionModel, wordRecord, index) {

        this.fireEvent('word-selected-in-list', wordRecord.get('word'));
        
    },
    
    onWordDeselect: function(selectionModel, wordRecord, index) {

        this.fireEvent('word-deselected-in-list', wordRecord.get('word'));
        
    },
    
    onWordAdded: function(word) {
        
        var wordList = Ext.ComponentQuery.query('word-list')[0];
        var store = wordList.getStore();
        
        if (store.findExact('word', word) === -1) {
            
            store.add({
                word: word
            });
            
        }
        
    }
    
});