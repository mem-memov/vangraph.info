Ext.define('Vangraph.controller.ClipGraphController', {
    
    extend: 'Ext.app.Controller',
    
    clipWords: {},
    wordClips: {},
    clips: [],
    words: [],
    clipWordEdges: {},
    wordClipEdges: {},
    loaded: [],
    playerIsOn: false,
    
    init: function() {
        
        this.listen({
            component: {
                'clip-graph': {
                    shapeclick: this.onShapeClick,
                    shapedblclick: this.onShapeDoubleClick,
                    beforeanimation: this.beforeAnimation,
                    shapemouseover: this.onShapeMouseOver,
                    shapemouseout: this.onShapeMouseOut,
                    disable: this.onClipGraphDisable
                }
            },
            controller: {
                '*': {
                    'new-word-query': this.onNewWordQuery,
                    'new-clip-query': this.onNewClipQuery,
                    'clip-player-shown': this.onClipPlayerShown,
                    'clip-player-hidden': this.onClipPlayerHidden,
                    'word-selected-in-list': this.onWordSelectedInList,
                    'word-deselected-in-list': this.onWordDeselectedInList
                }
            }
        });
        
    },
    
    onClipGraphDisable: function(clipGraph) {

        var maskDom = Ext.DomQuery.selectNode('.x-mask', Ext.getDom(clipGraph.getEl()));
        var maskExt = Ext.get(maskDom);
        //maskExt.setStyle('z-index', 50000);
        //clipGraph.toFront();
        maskExt.on('click', function() {
            
            this.fireEvent('player-removal-requested');
            
        }, this);
        
        
    },
    
    onShapeMouseOver: function(shape) {

        if (shape.xtype === 'vangraph-vertice-clip') {

            this.highlightClipWords(shape.id);
            this.contractSubtitle(shape.id);

        }
        
        if (shape.xtype === 'vangraph-vertice-word') {

            this.highlightWordClips(shape.id);
            this.contractClips(shape.id);

        }
        
    },
    
    onShapeMouseOut: function(shape) {

        if (shape.xtype === 'vangraph-vertice-word') {

            this.unhighlightWordsAndClips();
            this.expandClips(shape.id);
            
        }
        
        if (shape.xtype === 'vangraph-vertice-clip') {

            this.unhighlightWordsAndClips();
            this.expandSubtitle(shape.id);
            
        }
        
    },
    
    beforeAnimation: function(clipGraph) {
        
        return this.fireEvent('before-clip-graph-animation');
        
    },
    
    onClipPlayerShown: function() {

        this.playerIsOn = true;
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        clipGraph.stopAnimation();
        clipGraph.disable();

    },
    
    onClipPlayerHidden: function() {

        this.playerIsOn = false;
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        clipGraph.enable();
        clipGraph.startAnimation();
        
    },
    
    onShapeDoubleClick: function(shape, cmp) {
        
        if (typeof shape.id === 'undefined') {
            return;
        }
        
        if (shape.xtype === 'vangraph-vertice-clip') {
            
            var url = shape.url.replace('40', '480');
            
            this.fireEvent('clip-playback-requested', url);

        }
        
    },
    
    onShapeClick: function(shape, cmp) {

        if (typeof shape.id === 'undefined') {
            return;
        }

        if (shape.xtype === 'vangraph-vertice-word') {

            this.fireEvent('word-click', shape.id);
            
        }
        
        if (shape.xtype === 'vangraph-vertice-clip') {

            this.fireEvent('clip-click', shape.id);

        }
        
    },
    
    onNewWordQuery: function(query) {

        if (this.loaded.indexOf(query) !== -1) {
            this.highlightWordClips(query);
        } else {
            this.loadClips(query);
        }

    },
    
    onNewClipQuery: function(clipId) {

        if (this.clips.indexOf(clipId) !== -1) {

            var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
            this.unhighlightWordsAndClips();
            clipGraph.getVertice(clipId).togglePlayBack();
            this.loadWordsWithoutClip(clipId);
            
        } else {
            
            this.loadClipAndWords(clipId, function(sprite) {
                
                sprite.play();
                
            });
            
        }
        
    },
    
    // TODO: Think about a separate store
    
    loadClips: function(word) {
        
        var url = '/server/fetch/clips/of/word/'+word+'/limit/height/40/';

        if (window.location.hostname === 'localhost') { // in sencha workspace - localhost:1841
             url = 'http://localhost:3000/fetch/clips/of/word/'+word+'/limit/height/40/';
        }        
                
        Ext.Ajax.request({
            url: url,
            success: onClips,
            scope: this
        });
        
        function onClips (response) {
            
            var data = Ext.JSON.decode(response.responseText);

            var sprites = [];

            sprites.push({
                id: word,
                text: word,
                fill: 'green',
                isVertice: true,
                type: 'Word'
            });

            Ext.Array.each(data, function(clip) {

                sprites.push({
                    id: clip.id,
                    file: clip.file,
                    isVertice: true,
                    type: 'Clip'
                });

                sprites.push({
                    from: word,
                    to: clip.id,
                    isEdge: true,
                    stroke: 'green',
                    'stroke-width': 1,
                    type: 'Line'
                });

            });

            var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
            clipGraph.updateSprites(sprites);
            
            if (this.words.indexOf(word) === -1) {
                this.words.push(word);
                this.fireEvent('word-added', word);
            }
            
            this.wordClips[word] = [];
            Ext.Array.each(data, function(clip) {
                this.wordClips[word].push(clip.id);
                if (!this.clipWords[clip.id]) {
                    this.clipWords[clip.id] = [];
                }
                if (this.clipWords[clip.id].indexOf(word) === -1) {
                    this.clipWords[clip.id].push(word);
                }
                if (this.clips.indexOf(clip.id) === -1) {
                    this.clips.push(clip.id);
                }
            }, this);
            
            if (this.loaded.indexOf(word) !== -1) {
                this.loaded.push(word);
            }

            this.highlightWordClips(word);

        }

    },

    loadClipAndWords: function(clip, callback) {

        var url = '/server/fetch/clip/'+clip+'/';

        if (window.location.hostname === 'localhost') { // in sencha workspace - localhost:1841
             url = 'http://localhost:3000/fetch/clip/'+clip+'/';
        }  

        Ext.Ajax.request({
            url: url,
            scope: this,
            success: function(response) {

                var json = Ext.JSON.decode(response.responseText);

                if (!json.clip) {
                    return;
                }

                var sprite = {
                    id: json.clip.id,
                    file: json.clip.file,
                    isVertice: true,
                    type: 'Clip'
                };

                var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
                clipGraph.updateSprites([sprite]);
                
                if (typeof callback === 'function') {
                    callback(clipGraph.getVertice(sprite.id));
                }

                this.loadWordsWithoutClip(json.clip.id);

            }
        });

    },
    
    loadWordsWithoutClip: function(clip) {

        var url = '/server/fetch/words/of/clip/'+clip+'/';

        if (window.location.hostname === 'localhost') { // in sencha workspace - localhost:1841
             url = 'http://localhost:3000/fetch/words/of/clip/'+clip+'/';
        }  

        Ext.Ajax.request({
            url: url,
            scope: this,
            success: function(response) {

                var data = Ext.JSON.decode(response.responseText);

                var sprites = [], previousWord, lastIndex = data.length - 1;

                Ext.Array.each(data, function(word, index) {

                    sprites.push({
                        id: word.id,
                        text: word.id,
                        fill: 'green',
                        isVertice: true,
                        type: 'Word'
                    });

                    if (index === 0 || index === lastIndex) {
                        sprites.push({
                            from: word.id,
                            to: clip,
                            isEdge: true,
                            stroke: 'green',
                            'stroke-width': 1,
                            type: 'Line'
                        });
                    }

                    if (previousWord) {

                        sprites.push({
                            from: previousWord.id,
                            to: word.id,
                            isEdge: true,
                            stroke: 'grey',
                            'stroke-width': 1,
                            type: 'Line'
                        });

                    }

                    previousWord = word;

                });

                var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
                clipGraph.updateSprites(sprites);

                if (this.clips.indexOf(clip) === -1) {
                    this.clips.push(clip);
                }

                this.clipWords[clip] = [];
                Ext.Array.each(data, function(word) {
                    this.clipWords[clip].push(word.id);
                    if (!this.wordClips[word.id]) {
                        this.wordClips[word.id] = [];
                    }
                    if (this.wordClips[word.id].indexOf(clip) === -1) {
                        this.wordClips[word.id].push(clip);
                    }
                    if (this.words.indexOf(word.id) === -1) {
                        this.words.push(word.id);
                        this.fireEvent('word-added', word.id);
                    }
                }, this);

                if (this.loaded.indexOf(clip) !== -1) {
                    this.loaded.push(clip);
                }

                this.highlightClipWords(clip);
                this.contractSubtitle(clip);

            }
        });

    },

    highlightClipWords: function(clip) {
        
        this.unhighlightWordsAndClips();
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];

        clipGraph.getVertice(clip).highlight();

        Ext.Array.each(this.clipWords[clip], function(word) {
            clipGraph.getVertice(word).highlight();
        }, this);
        
    },
    
    highlightWordClips: function(word) {
        
        this.unhighlightWordsAndClips();
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        
        clipGraph.getVertice(word).highlight();

        Ext.Array.each(this.wordClips[word], function(clip) {
            clipGraph.getVertice(clip).highlight();
        }, this);
        
    },
    
    unhighlightWordsAndClips: function() {
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];

        Ext.Array.each(this.words, function(word) {
            clipGraph.getVertice(word).unhighlight();
        }, this);

        Ext.Array.each(this.clips, function(clip) {
            clipGraph.getVertice(clip).unhighlight();
        }, this);
        
    },
    
    contractSubtitle: function(clip) {
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        
        var edges = this.findSubtitleEdges(clip);

        Ext.Array.each(edges, function(edge) {

            if (edge) {
                clipGraph.vangraph.insertSpring({
                    id1: edge.from,
                    id2: edge.to, 
                    stiffness: 0.01,
                    lengthAtRest: 10
                });
            }
            
        }, this);
        
    },
    
    expandSubtitle: function(clip) {
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        
        var edges = this.findSubtitleEdges(clip);

        Ext.Array.each(edges, function(edge) {

            if (edge) {
                clipGraph.vangraph.insertSpring({
                    id1: edge.from,
                    id2: edge.to, 
                    stiffness: 0.0001,
                    lengthAtRest: 50
                });
            }
            
        }, this);
        
    },
    
    findSubtitleEdges: function(clip) {
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        
        if (!this.clipWordEdges[clip] || this.loaded.indexOf(clip) === -1) {
            
            this.clipWordEdges[clip] = [];
            
            Ext.Array.each(this.clipWords[clip], function(current, index) {
                
                if (index === 0) {
                    return;
                }
                
                var previous = this.clipWords[clip][index-1];
                
                var edge = clipGraph.getEdge(previous, current);
                
                this.clipWordEdges[clip].push(edge);
                
            }, this);
            
        }
        
        return this.clipWordEdges[clip];
        
    },
    
    contractClips: function(word) {
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        
        var edges = this.findWordClipEdges(word);
        
        Ext.Array.each(edges, function(edge) {

            if (edge) {
                clipGraph.vangraph.insertSpring({
                    id1: edge.from,
                    id2: edge.to, 
                    stiffness: 0.0008,
                    lengthAtRest: 100
                });
            }
            
        }, this);
        
    },
   
    expandClips: function(word) {
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        
        var edges = this.findWordClipEdges(word);
        
        Ext.Array.each(edges, function(edge) {

            if (edge) {
                clipGraph.vangraph.insertSpring({
                    id1: edge.from,
                    id2: edge.to, 
                    stiffness: 0.0001,
                    lengthAtRest: 50
                });
            }
            
        }, this);
        
    },
   
    findWordClipEdges: function(word) {
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        
        if (!this.wordClipEdges[word] || this.loaded.indexOf(word) === -1) {
            
            this.wordClipEdges[word] = [];
            
            Ext.Array.each(this.wordClips[word], function(clip, index) {

                var edge = clipGraph.getEdge(word, clip);
                
                this.wordClipEdges[word].push(edge);
                
            }, this);
            
        }
        
        return this.wordClipEdges[word];
        
    },
    
    onWordSelectedInList: function(word) {
        
        var clipGraph = Ext.ComponentQuery.query('clip-graph')[0];
        clipGraph.panToWord(word);
        this.highlightWordClips(word);
        clipGraph.startAnimation();
        this.contractClips(word);
        
    },
    
    onWordDeselectedInList: function(word) {
        
        this.expandClips(word);
        this.unhighlightWordsAndClips();
        
    }
   
});