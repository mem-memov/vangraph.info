Ext.define('Vangraph.store.WordStore', {
    
    extend: 'Ext.data.Store',
    
    alias: 'store.word-store',
    
    model: 'Vangraph.model.WordModel',
    
    proxy: {
        type: 'memory'
    },
    
    sorters: [
        { property: 'word' }
    ]
    
});