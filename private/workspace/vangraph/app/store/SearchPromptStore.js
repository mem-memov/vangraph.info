Ext.define('Vangraph.store.SearchPromptStore', {
    
    extend: 'Ext.data.Store',
    
    alias: 'store.search-prompt-store',
    
    model: 'Vangraph.model.SearchPromptModel',
    
    proxy: {
        type: 'rest',
        buildUrl: function(request) {
            
            var url = '/server/prompt/input/'+request.params.query+'/';
            
            if (window.location.hostname === 'localhost') { // in sencha workspace - localhost:1841
                 url = 'http://localhost:3000/prompt/input/'+request.params.query+'/';
            }   

            return url;
            
        },
        reader: {
            type: 'json'
        }
    }
    
});