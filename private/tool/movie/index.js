var program = require('commander-plus');
var async = require('async');
var fs = require('fs');
var path = require('path');
var dvdImporter = require('./dvdImporter');
var videoCutter = require('./videoCutter');

var masterDirectory = path.normalize(__dirname + '/../../../data/video/master');

chooseFromMenu();

function chooseFromMenu() {
    
    var options = [
        "Import DVD",
        "Cut copies"
    ];

    var handlers = [
        importDVD,
        cutCopies
    ];
    
    program.choose(options, function(index) {

        if (typeof handlers[index] !== 'undefined') {
            handlers[index]();
        }

    });
    
}

function importDVD() {

    selectVideoDirectory(function(names, paths) {
        
        program.choose(names, function(index) {

            dvdImporter.import(names[index], paths[index]);

        });
        
    });
    
}

function cutCopies() {
    
    selectVideoDirectory(function(names, paths) {
        
        program.choose(names, function(index) {

            videoCutter.cut(names[index], paths[index]);

        });
        
    });
    
}

function selectVideoDirectory(callback) {

    program.prompt('Enter a word from the film name to find it inside /data/video/master:' + ' ', function(query) {

        fs.readdir(masterDirectory, function(error, names) {
            
            if (error) throw error;
            
            query = query.toLowerCase();
            var selectedNames = [];
            var selectedPaths = [];
            
            names.forEach(function(name) {

                if (name.indexOf(query) !== -1) {
                    selectedNames.push(name);
                    selectedPaths.push(masterDirectory+'/'+name);
                }
                
            });
            
            if (selectedNames.length === 0) {
                selectVideoDirectory();
            } else {
                callback(selectedNames, selectedPaths);
            }
            
        });
        
    });

}