var fs = require('fs');
var crypto = require('crypto');

/**
 * 
 * @param Array copies [ copy, copy ]
 * @param Array cuts [ [ clip, clip, clip ], [ clip, clip, clip ] ]
 * @param Object languageSubtitles { russian: [ sub, sub, sub ], english: [ sub, sub, sub ] }
 * @returns {undefined}
 */
exports.create = function(copies, cuts, languageSubtitles, master, directoryPath, directoryName) {
    
    var queries = [];
    
    cuts.forEach(function(cut, cutIndex) {

        var copy = copies[cutIndex];

        cut.forEach(function(clip, clipIndex) {
            
            var subtitle = languageSubtitles[copy.language][clipIndex];
            
            var words = getWords(subtitle.lines.join(' '));
            
            var wordParts = [];
            
            words.forEach(function(word, wordIndex) {
                
                wordParts.push(
                    '(clip)' + 
                    '-[:clip_word]->' + 
                    '(word' + wordIndex + ':word {' +
                        'string: "' + word + '", ' +
                        'position: ' + wordIndex +
                    '})'
                );
        
                if (wordIndex > 0) {
                    
                    wordParts.push(
                        '(word' + (wordIndex-1) + ')' +
                        '-[:next_word]->' +
                        '(word' + wordIndex + ')'
                    );
                    
                }
                
            });
            
            var md5sum = crypto.createHash('md5');
            md5sum.update('/' + directoryName + '/mp4/h' + copy.height + '/' +  copy.language + '/' +  clip.file);
            
            queries.push(
                'CREATE ' +
                    '(clip:clip {' +
                        'id: "' + md5sum.digest('hex') + '", ' + 
                        'position: ' + clipIndex + ', ' +
                        'start: ' + clip.start + ', ' + 
                        'stop: ' + clip.stop + ', ' + 
                        'duration: ' + clip.duration + ', ' + 
                        'height: ' + copy.height + ', ' + 
                        'language: "' +copy.language + '", ' +
                        'file: "' + clip.file + '", ' +
                        'directory: "' + directoryName + '"' +
                    '})' +
                    (wordParts.length > 0 ? ', ' : '') +
                    wordParts.join(', ') +
                    (clipIndex > 0 ? ' WITH clip MERGE (clip)<-[:next_clip]-(:clip {position: '+(clipIndex-1)+'})' : '') +
                ';'
            );
            
        });

    });
    
    save(directoryPath, queries);
    
};

function getWords(input) {

	input = input.toLowerCase();
	input = input.replace(/[-,:;\.\(\)\- \[\]\?\!–]{1,}/g, ' ');

	input = input.replace(/'/g, '\\\'');
	input = input.replace(/^\s/g, '');
	input = input.replace(/\s$/g, '');
	
	return input.split(' ');
	
}
function save(directoryPath, queries) {
    
    var path;
    
    path = directoryPath + '/neo4j_film_description.cql';
    
    fs.writeFile(
        path, 
        queries.join('\n')+'\n', 
        function (error) {
            if (error) throw error;
            console.log('Query is inside '+path);
        }
    );


    var chankMaxSize = 5000;
    var chunks = [], chunk = [];
    
    queries.forEach(function(query) {
        
        chunk.push(query);
        
        if (chunk.length === chankMaxSize) {
            
            chunks.push(chunk);
            chunk = [];
  
        }
        
    });
    if (chunk.lenght > 0) {
        chunks.push(chunk);
        chunk = [];
    }
    
    chunks.forEach(function(chunk, index) {
        
        fs.writeFile(
            directoryPath + '/neo4j_film_description_'+index+'.cql', 
            chunk.join('\n')+'\n', 
            function (error) {
                if (error) throw error;
            }
        );
        
    });
    
}

// scp /home/novikov/NetBeansProjects/vangraph/www/data/video/master/harry_potter_and_the_sorcerer_s_stone/neo4j_film_description.cql root@vangraph.info:/root/tmp