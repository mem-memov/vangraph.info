var program = require('commander-plus');
var fs = require('fs');
var path = require('path');
var exec = require('child_process').exec;
var async = require('async');
var json = require('format-json');
var cypherQueryCreator = require('./cypherQueryCreator');

exports.cut = function(directoryName, directoryPath) {
    
    var metaFilePath = directoryPath + '/meta.json';  
    
    createMetaFileIfAbsent(directoryName, metaFilePath, function() {
        
        fs.readFile(metaFilePath, {encoding: 'UTF-8'}, function(error, data) {
            
            if (error) throw error;
            
            var metaData = JSON.parse(data);
            
            readSubtitleFiles(metaData, function(languageSubtitles) {

                var copyCommandsAndInfos = cutCopies(languageSubtitles, metaData);
                
                // cut video files
                var commands = [];
                copyCommandsAndInfos.forEach(function(commandsAndInfos) {
                    commands.push.apply(commands, commandsAndInfos.commands);
                });
                //executeCutCommands(commands);

                // create database queries
                var cutInfos = [];
                copyCommandsAndInfos.forEach(function(commandsAndInfos) {
                    cutInfos.push(commandsAndInfos.cutInfos);
                });
                cypherQueryCreator.create(
                    metaData.copies, 
                    cutInfos, 
                    languageSubtitles, 
                    metaData.source.video,
                    directoryPath,
                    directoryName
                );
                
            });
            
        });
        
    });
    
};

function createMetaFileIfAbsent(directoryName, metaFilePath, callback) {
    
    fs.exists(metaFilePath, function(exists) {
        
        if (exists) {
            
            callback();
            
        } else {
            
            var meta = json.plain(createMetaDataExample(directoryName));
            
            fs.writeFile(metaFilePath, meta, function(error) {
                
                if (error) throw error;
                
                var message = 'Now make corrections in file ' + metaFilePath + ' Please confirm meta data is ready to be used for cuttion video (yes/no)'
                
                program.confirm(message, function(ok) {
                    
                    if (ok) {
                        callback();
                    } else {
                        process.exit();
                    }
                    
                });

            });
            
        }
        
    });
    
}
function createMetaDataExample(directoryName) {
    
    return {
        source: {
            video: {
                type: 'dvd',
                path: '/data/video/master/'+directoryName+'/dvd'
            },
            subtitles: [
                {
                    language: 'english',
                    path: '/data/video/master/'+directoryName+'/subtitles/english.srt'
                },
                {
                    language: 'russian',
                    path: '/data/video/master/'+directoryName+'/subtitles/russian.srt'
                }
            ]
        },
        copies: [
            {
                height: 40,
                language: 'english',
                path: '/data/video/copy/'+directoryName+'/mp4/h40/audio1/output.mp4'
            },
            {
                height: 480,
                language: 'english',
                path: '/data/video/copy/'+directoryName+'/mp4/h480/audio1/output.mp4'
            },
            {
                height: 40,
                language: 'russian',
                path: '/data/video/copy/'+directoryName+'/mp4/h40/audio0/output.mp4'
            },
            {
                height: 480,
                language: 'russian',
                path: '/data/video/copy/'+directoryName+'/mp4/h480/audio0/output.mp4'
            }
        ]
    };
    
}
function readSubtitleFiles(metaData, callback) {
    
    var languageSubtitles = {};

    async.eachSeries(metaData.source.subtitles, function(subtitleData, callback) {

        readSubtitleFile(subtitleData, function(subtitleObjects) {
            
            languageSubtitles[subtitleData.language] = subtitleObjects;
            
            callback();

        });
        
    }, function(error) {
        
        if (error) throw error;

        callback(languageSubtitles);
        
    });
    
}
function readSubtitleFile(subtitleData, callback) {

    var pathToSubtitleList = path.normalize(__dirname + '/../../..' + subtitleData.path);
    
    fs.readFile(pathToSubtitleList, {encoding: 'utf8'}, function (error, data) {

        if (error) throw error;

        data = data.replace(/\r/g, '');

        var subtitles = data.split('\n\n');
        var subtitleParts, line, subtitleObject, subtitleObjects = [], problemInSubtitle;

        for (var i=0, iLim=subtitles.length; i<iLim; i++) {

            subtitleParts = subtitles[i].split('\n');
            subtitleObject = {};
            problemInSubtitle = false;

            for (var k=0, kLim=subtitleParts.length; k<kLim; k++) {

                if (!subtitleParts[0]) {
                    problemInSubtitle = true;
                    break;
                }
                subtitleObject.number = subtitleParts[0];

                if (!subtitleParts[1]) {
                    problemInSubtitle = true;
                    break;
                }
                var periodPositionSize = subtitleParts[1].split(' ');
                if (!periodPositionSize[0] || !periodPositionSize[2]) {
                    problemInSubtitle = true;
                    break;
                }
                subtitleObject.start = periodPositionSize[0].split(',')[0];
                subtitleObject.end = periodPositionSize[2].split(',')[0];
                subtitleObject.lines = [];
                for (var m=2, mLim=subtitleParts.length; m<mLim; m++) {
                    line = subtitleParts[m];
                    if (line.indexOf('<') === -1 && line !== '') {
                        subtitleObject.lines.push(line);
                    }
                }
                if (subtitleObject.lines.length < 1) { 
                    problemInSubtitle = true;
                    break;
                }

          }

          if (!problemInSubtitle) {
              subtitleObjects.push(subtitleObject);
          }

        }

        callback(subtitleObjects);

    });

  
}
function cutCopies(languageSubtitles, metaData) {

    var commandsAndInfos, copyCommandsAndInfos = [];
    
    metaData.copies.forEach(function(copyData) {
        
        commandsAndInfos = cutCopy(copyData, languageSubtitles);
        
        copyCommandsAndInfos.push(commandsAndInfos);
        
    });

    return copyCommandsAndInfos;
    
}
function cutCopy(copyData, languageSubtitles) {

    var pathToSourceVideo = path.normalize(__dirname + '/../../..' + copyData.path);
    var resultDirectory = path.dirname(pathToSourceVideo) + '/cut';
    var commands = [], command, clipFileName, pathToResultVideo, duration, start, end;
    var subtitleObject, subtitleObjects = languageSubtitles[copyData.language];
    var cutInfo, cutInfos = [];
    
    commands.push('mkdir -p ' + resultDirectory + ' ');

    for(var n=0, nLim=subtitleObjects.length; n<nLim; n++) {
        
        cutInfo = {};

        subtitleObject = subtitleObjects[n];

        start = parseTime(subtitleObject.start);
        end = parseTime(subtitleObject.end);
        duration = end - start;

        clipFileName = 'from_'+secondsToTime(start).replace(/\D/g, '_')+'_to_'+secondsToTime(end).replace(/\D/g, '_')+'.mp4';
        
        pathToResultVideo = resultDirectory+'/' + clipFileName;

        if (duration > 20) {
            throw subtitleObject.number + ' subtitle  time too long: '+duration;
        }

        cutInfo.start = start-1 > 0 ? start-1 : 0;
        cutInfo.duration = duration+3;
        cutInfo.stop = cutInfo.start + cutInfo.duration;
        cutInfo.file = clipFileName;

        command = 'ffmpeg ' + 
                '-analyzeduration 2147480000 ' + // [0 - 2.14748e+09] - increase sampling limits
                '-probesize 2147480000 ' + // [32 - 2.14748e+09] increase sampling limits
                '-ss ' + cutInfo.start + ' ' +
                '-t ' + cutInfo.duration + ' ' + 
                '-i ' + pathToSourceVideo + ' ' + 
                '-y ' + // replace existing files
                pathToResultVideo
        ;
        
        commands.push(command);
        cutInfos.push(cutInfo);

    }

    return {
        commands: commands,
        cutInfos: cutInfos
    }
    
}
function executeCutCommands(commands, callback) {
    
    async.eachSeries(commands, function(command, callback) {

        executeCommand(command, function(error, stdout, stderr) {
            callback(error);
        });

    }, function(error) {

        if (error) throw error;

        callback();

    });
    
}
function executeCommand(command, callback) {

	var child = exec(command, function (error, stdout, stderr) {
        
        console.log(command);
		//console.log(stdout);
		//console.log(stderr);
        
		callback && callback(error, stdout, stderr);
        
	});
	
}
function parseTime(time) {
   var parts = time.split(':');
   return parseInt(parts[0],10) * 60 * 60 + parseInt(parts[1],10) * 60 + parseInt(parts[2],10);
}
function secondsToTime(seconds) {
	var hours = Math.floor(seconds / 3600);
	var minutes = Math.floor((seconds - hours*3600) / 60);
	var rest = seconds - hours*3600 - minutes * 60;
	return (hours < 10 ? '0'+hours : ''+hours) + ':' + (minutes < 10 ? '0'+minutes : ''+minutes) + ':' + (rest < 10 ? '0'+rest : ''+rest);
}