var program = require('commander-plus');
var fs = require('fs');
var path = require('path');
var exec = require('child_process').exec;
var async = require('async');

exports.import = function(directoryName, directoryPath) {
    
    var dvdDirectory = directoryPath + '/dvd';
    
    fs.exists(dvdDirectory, function(exists) {
        
        if (!exists) {
            
            fs.mkdir(dvdDirectory, function(error) {
                
                if (error) throw error;
                
                selectFiles(dvdDirectory, directoryName);
                
            });
            
        } else {
            
            selectFiles(dvdDirectory, directoryName);
            
        }
        
    });
    
};

function selectFiles(dvdDirectory, directoryName) {
    
    fs.readdir(dvdDirectory, function(error, names) {

        var vobFiles = [];
        var vobPaths = [];

        names.forEach(function(name) {

            if (/\.vob$/ig.test(name)) {

                vobFiles.push(name);
                vobPaths.push(dvdDirectory + '/' + name);

            }

        });
        
        vobFiles.sort();
        vobPaths.sort();
        
        console.log(vobFiles);
        console.log(vobPaths);
        
        readInfo(vobFiles, vobPaths, directoryName);

    });
    
}

function readInfo(vobFiles, vobPaths, directoryName) {
    
    var index = 2;
    var vobPath = vobPaths[index];
    var mustHave, audioTokens=[], videoTokens=[];
    var command = 'ffprobe -loglevel info -i ' + vobPath;
    
    executeCommand(command, function(error, stdout, stderr) {
        
        mustHave = detectStreams(stderr);
        
        for (var audioToken in mustHave.audio) {
            if (mustHave.audio.hasOwnProperty(audioToken)) {
                audioTokens.push(audioToken);
            }
        }
        
        for (var videoToken in mustHave.video) {
            if (mustHave.video.hasOwnProperty(videoToken)) {
                videoTokens.push(videoToken);
            }
        }

        var filteredVobPaths = [];
        var filteredStreams = [];
        
        async.eachSeries(vobPaths, function(vobPath, callback) {
            
            var command = 'ffprobe -loglevel info -i ' + vobPath;

            executeCommand(command, function(error, stdout, stderr) {

                var has = detectStreams(stderr);
                var ok = true;

                audioTokens.forEach(function(audioToken) {
                    if (!has.audio[audioToken]) {
                        ok = false;
                    }
                });
                videoTokens.forEach(function(videoToken) {
                    if (!has.video[videoToken]) {
                        ok = false;
                    }
                });

                if (ok) {
                    filteredVobPaths.push(vobPath);
                    filteredStreams.push(has);
                }

                callback();

            });

        }, function(error) {
            
            if (error) throw error;

            resize(videoTokens, audioTokens, filteredVobPaths, filteredStreams, directoryName)

        });
        
    });

}

function executeCommand(command, callback) {
	
	console.log(command);
	
	var child = exec(command, function (error, stdout, stderr) {
        
		console.log(stdout);
		console.log(stderr);
        
		callback && callback(error, stdout, stderr);
        
	});
	
}

function detectStreams(stderr) {
   
    var matches = stderr.match(/Stream #.*?\[.*?\]\:.*?\:/g);
    
    var id, token, type, parts, audioStreams = [], videoStreams = [];
    
    matches.forEach(function(match) {
        
        parts = /^.*?(\d+\:\d+)\[(.*?)\]\:\s(\w+)/.exec(match);
        
        id = parts[1];
        token = parts[2];
        type = parts[3].toLowerCase();
        
        switch (type) {
            case 'audio':
                audioStreams[token] = id;
                break;
            case 'video':
                videoStreams[token] = id;
        }

    });

    return {
        video: videoStreams,
        audio: audioStreams
    };
    
}

function resize(videoTokens, audioTokens, vobPaths, streams, directoryName) {

    var outputDirectory, commands = [], height, audioId, videoId;
    var copyDirectory = path.normalize(__dirname + '/../../../data/video/copy/' + directoryName);
    var heights = [40, 480];
    var commandFile = path.normalize(__dirname + '/../../../data/video/master/' + directoryName + '/commands.log');
    

    heights.forEach(function(height) {
        
        audioTokens.forEach(function(audioToken, audioIndex) {

            var outputDirectory = copyDirectory + '/mp4/h' + height + '/audio' + audioIndex;
            var outputFileNames=[], outputFiles = [];
            var partListFile = outputDirectory + '/parts.txt';

            vobPaths.forEach(function(vobPath, pathIndex) {

                audioId = streams[pathIndex].audio[audioToken];
                videoId = streams[pathIndex].video[videoTokens[0]];
                var outputFileName = 'output_' + pathIndex + '.mp4';
                outputFileNames.push(outputFileName);
                var outputFile = outputDirectory + '/' + outputFileName;
                outputFiles.push(outputFile);

                commands.push(
                    'mkdir -p ' + outputDirectory + ' && ' +
                    'ffmpeg ' +
                    '-analyzeduration 2147480000 ' + // [0 - 2.14748e+09] - increase sampling limits
                    '-probesize 2147480000 ' + // [32 - 2.14748e+09] increase sampling limits
                    '-i ' + vobPath + ' ' +
                    '-strict experimental ' + // for acc format
                    // '-y ' + // replace existing files
                    '-filter:v scale=-1:' + height + ' ' +
                    '-sn ' + // do not write subtitles
                    '-map ' + videoId + ' ' +
                    '-map ' + audioId + ' ' +
                    '-c:v libx264 ' +
                    '-c:a aac -b:a 128k -ac 2 ' +
                    outputFile
                );

            });

            fs.writeFileSync(partListFile, 'file \'' + outputFileNames.join('\'\nfile \'')+'\'');
            
            commands.push('ffmpeg -f concat -i '+partListFile+' -c copy '+outputDirectory + '/output.mp4');

        });
    
    });
    
    fs.writeFileSync(commandFile, commands.join('\n'));

    async.eachSeries(commands, function(command, callback) {

        executeCommand(command, function(error, stdout, stderr) {
            callback(error);
        });

    }, function(error) {

        if (error) throw error;

        console.log('DONE!');
        
        process.exit();

    });
    
}
