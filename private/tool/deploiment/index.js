var program = require('commander-plus');
var exec = require('child_process').exec;
var path = require('path');
var async = require('async');

var options = [
    "Build ExtJs project and export it to public directory"
];

var handlers = [
    buildExtJsProject
];

program.choose(options, function(index){
    
    process.stdin.destroy();
    
    if (typeof handlers[index] !== 'undefined') {
        handlers[index]();
    }

});

function buildExtJsProject() {
    
    var projectPath = path.normalize(__dirname + '/../../workspace/vangraph/app');

    var build = exec(
            'sencha app build',  
        { 
            stdio: 'inherit',
            cwd: projectPath 
        }, 
        function (error, stdout, stderr) {
            
            if (error) throw error;
            
            console.log('stdout: ' + stdout);
            
            clearPublicDirectory();
            
        }
    );

    
};

function clearPublicDirectory() {
    
    var publicPath = path.normalize(__dirname + '/../../../public/client');
    
    var remove = exec(
            'rm -r ./*',  
        { 
            stdio: 'inherit',
            cwd: publicPath 
        }, 
        function (error, stdout, stderr) {
            
            if (error) throw error;
            
            copyExtJsProject();
            
        }
    );
    
}

function copyExtJsProject() {
    
    var productionPath = path.normalize(__dirname + '/../../workspace/build/Vangraph/production');
    var publicPath = path.normalize(__dirname + '/../../../public/client');
    
    var copy = exec(
            'cp -r . ' + publicPath,  
        { 
            stdio: 'inherit',
            cwd: productionPath 
        }, 
        function (error, stdout, stderr) {
            
            if (error) throw error;
            
        }
    );
    
}

